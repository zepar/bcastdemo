# README #

**Remote "Selfie" or "Grupie" triggering app for Android.** Take great gropu pictures with your own smartphone from a friend�s smartphone 

	This README is to document the steps necessary to get the application up and running.

* **Quick summary**

This is intended to demo remote triggering of the camera in a mobile phone from another mobile phone

NOTE: WiFi P2P validation works OK but I have only one WiFi Direct certified device so I cannot test communication since the emulator does not support WiFi Direct.
I will create a new Git Branch to try BLUETOOTH


* **Features**

	* One app with two modes
		1 server mode corresponds to the device to take a picture
		2 client mode is the device from where the picture is to be triggered
	* The client app communicates to the server one over WiFi
	* Live video from server camera on both Server and Client screens.
	* Server camera can be setup or positioned correctly before picture triggering
	* The actual video on the server app can be seen in the client app 

* Version

**0.01** Sept 20, 2017  initial camera setup

### Set up ###

* **Summary of set up**

	* Android Studio 3.0 
	* 2 Android smartphones, at least one with front face camera
	* WiFi network available
	

* **Configuration**


* **Dependencies**
	* Developer mode should be enabled in the smartphone devices
	* ROOM storage


* **Database configuration**
	* Application is initially developed with ROOM for local storage of the pictures taken


* How to run tests
	* Download the code from this git repository
	* Simply connect your Android smartphone to your laptop and select the device when running the app


* Deployment instructions


### Contribution guidelines ###

* Writing tests
* Code review
* References
	* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)



### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact